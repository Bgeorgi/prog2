 
 class Madar{
	 void repul(){
		System.out.println("Tud repülni.\n");
	}
}

class Sas extends Madar{
	@Override
	void repul(){
		System.out.println("A sas repül.\n");
	}
}

class Pingvin extends Madar{

}

class Liskov{
	public static void main(String[] args) {
		Madar madar=new Madar();
		madar.repul();
		madar=new Sas();
		madar.repul();
		madar=new Pingvin();
		madar.repul();
	}
}